<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  if (isset($_POST['submit'])) {
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    $nama = trim($_POST['nama']);
    $no_hp = trim($_POST['no_hp']);
    $tgl_lahir = trim($_POST['tgl_lahir']);
    $departemen = trim($_POST['departemen']);
    $posisi = trim($_POST['posisi']);
    $email = trim($_POST['email']);
    $jenis_kelamin = trim($_POST['jenis_kelamin']);
    $password = ($_POST['password']);
    $password2 = ($_POST['password2']);
    $password3 = (md5($password));

    function ubahTanggal($tgl_lahir){
    $pisah = explode('/',$tgl_lahir);
    $array = array($pisah[2],$pisah[1],$pisah[0]);
    $satukan = implode('-',$array);
     return $satukan;
    }

    $tgl_lahir2 = ubahTanggal($tgl_lahir);

    $alfabet = preg_match('@[a-zA-Z]@', $password);
    $number = preg_match('@[0-9]@', $password);

    $cek = "SELECT email FROM tb_user WHERE email='$email'";
    $cekqry = mysqli_query($koneksi, $cek);
    $cekrow = mysqli_fetch_array($cekqry);

    if (preg_match("/HC OFFICER/", $posisi)) {
      echo $login_status = "1";
    }else{
      echo $login_status = "2";
    }
  
    if (empty($nama) && empty($no_hp) && empty($tgl_lahir) && empty($departemen) && empty($position) && empty($email) && empty($jenis_kelamin) && empty($password) && empty($password2)) {
      echo "<script>alert('Data masih kosong!');history.go(-1)</script>";
    }elseif (empty($nama)) {
      echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
    }elseif (empty($no_hp)) {
      echo "<script>alert('Number Phone harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_lahir)) {
      echo "<script>alert('Date of Birth harus di isi!');history.go(-1)</script>";
    }elseif (empty($departemen)) {
      echo "<script>alert('Departement harus di isi!');history.go(-1)</script>";
    }elseif (empty($posisi)) {
      echo "<script>alert('Silahkan pilih Position!');history.go(-1)</script>";
    }elseif (empty($email)) {
      echo "<script>alert('Email harus di isi!!');history.go(-1)</script>";  
    }elseif (empty($jenis_kelamin)) {
      echo "<script>alert('Silahkan pilih Gender!');history.go(-1)</script>";
    }elseif (empty($password)) {
      echo "<script>alert('Password harus di isi!');history.go(-1)</script>";
    }elseif (empty($password2)) {
      echo "<script>alert('Confirmation Password harus di isi!');history.go(-1)</script>";
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
      echo "<script>alert('Name tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
    }elseif (strlen($nama) >= 60) {
      echo "<script>alert('Panjang Name tidak boleh 60 Character!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-9]*$/", $no_hp)) {
      echo "<script>alert('Number Phone tidak boleh mengandung special character atau huruf!');history.go(-1)</script>";
    }elseif (strlen($no_hp) <= 9 || strlen($no_hp) >= 14) {
      echo "<script>alert('Number Phone hanya boleh 10-13 Angka!');history.go(-1)</script>";
    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      echo "<script>alert('Format email yang anda masukkan salah!');history.go(-1)</script>";
    }elseif($cekrow['email'] == $email){
      echo "<script>alert('Email yang anda masukkan sudah ada!');history.go(-1)</script>";
    }elseif (!$alfabet ||  !$number || strlen($password) <= 5) {
      echo "<script>alert('Password  harus mengandung angka dan huruf, tidak boleh special character & lebih 6 panjang character!');history.go(-1)</script>";
    }elseif ($password != $password2) {
      echo "<script>alert('Confirm Password tidak sama!');history.go(-1)</script>";
    }elseif ($jenis_kelamin != "Female" && $jenis_kelamin != "Male") {
      echo "<script>alert('Gender Salah!');history.go(-1)</script>";
    }else{
      $sql2 = "INSERT INTO tb_user (email,login_status,password,nama,no_hp,tgl_lahir,departemen,posisi,jenis_kelamin,tgl_daftar) VALUES ('$email','$login_status','$password3','$nama','$no_hp','$tgl_lahir2','$departemen','$posisi','$jenis_kelamin',current_timestamp())";
      $qry2 = mysqli_query($koneksi, $sql2) or die ("query insert salah");
        echo "<script>alert('User Id telah berhasil di tambahkan.');window.location='updateuserid.php'; </script>";
    }
  }



  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User Id</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="adduserid.php">Manage User Id</a></li>
              <li class="breadcrumb-item active">Add User Id</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form action="" name="submit" method="post">
        <div class="container-fluid">
          <!-- SELECT2 EXAMPLE -->
          <div class="card card-olive">
            <div class="card-header">
              <h3 class="card-title">New User Id</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input required name="nama" type="text" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Number Phone</label>
                      <div class="input-group">
                        <input required minlength="11" maxlength="14" name="no_hp" type="text" class="form-control border-list-olive">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Date of Birth</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input required name="tgl_lahir" type="text" class="form-control datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Departement</label>
                    <select name="departemen" class="form-control select2" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="BD">BD</option>
                      <option value="CONS">CONS</option>
                      <option value="DGMKT">DGMKT</option>
                      <option value="EM">EM</option>
                      <option value="FA">FA</option>
                      <option value="HCGA">HCGA</option>
                      <option value="LEGP">LEGP</option>
                      <option value="LND">LND</option>
                      <option value="MCOM">MCOM</option>
                      <option value="MIS">MIS</option>
                      <option value="MKTDA">MKTDA</option>
                      <option value="PDG">PDG</option>
                      <option value="QS">QS</option>
                      <option value="SALES">SALES</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Position</label>
                    <select name="posisi" class="form-control select2 border-list-olive" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="ACCOUNTING ADMINISTRATOR">ACCOUNTING ADMINISTRATOR</option>
                      <option value="ACCOUNTING SUPERVISOR">ACCOUNTING SUPERVISOR</option>
                      <option value="ADMIN ACCONTING">ADMIN ACCONTING</option>
                      <option value="ADMIN COLLECTION">ADMIN COLLECTION</option>
                      <option value="ADMIN PURNA JUAL">ADMIN PURNA JUAL</option>
                      <option value="AFTER SALES ADMINISTATION">AFTER SALES ADMINISTATION</option>
                      <option value="AFTER SALES SUPERVISOR">AFTER SALES SUPERVISOR</option>
                      <option value="ARCHITECT">ARCHITECT</option>
                      <option value="ASSISTANT ACCOUNTING MANAGER">ASSISTANT ACCOUNTING MANAGER</option>
                      <option value="ASSISTANT COLLECTION MANAGER">ASSISTANT COLLECTION MANAGER</option>
                      <option value="ASSISTANT ESTATE MANAGEMENT MANAGER">ASSISTANT ESTATE MANAGEMENT MANAGER</option>
                      <option value="ASSISTANT QUANTITY SURVEYOR MANAGER">ASSISTANT QUANTITY SURVEYOR MANAGER</option>
                      <option value="ASSOCIATE DIRECTOR">ASSOCIATE DIRECTOR</option>
                      <option value="ASSOCIATE DIRECTOR 1">ASSOCIATE DIRECTOR 1</option>
                      <option value="BUILDING COORDINATOR">BUILDING COORDINATOR</option>
                      <option value="BUILDING INSPECTOR">BUILDING INSPECTOR</option>
                      <option value="BUILDING SITE MANAGER">BUILDING SITE MANAGER</option>
                      <option value="BUSINESS DEVELOPMENT OFFICER">BUSINESS DEVELOPMENT OFFICER</option>
                      <option value="CASHIRE">CASHIRE</option>
                      <option value="COLLECTION ADMINISTRATOR">COLLECTION ADMINISTRATOR</option>
                      <option value="COLLECTION OFFICE">COLLECTION OFFICE</option>
                      <option value="COMMERCIAL AREA INSPECTOR">COMMERCIAL AREA INSPECTOR</option>
                      <option value="CONSTRUCTION ADMINISTRATOR">CONSTRUCTION ADMINISTRATOR</option>
                      <option value="CONSTRUCTION MANAGER">CONSTRUCTION MANAGER</option>
                      <option value="CUSTOMER SERVICE">CUSTOMER SERVICE</option>
                      <option value="DESIGN GRAPHIC">DESIGN GRAPHIC</option>
                      <option value="DIGITAL MARKETING MANAGER">DIGITAL MARKETING MANAGER</option>
                      <option value="DIGITAL MARKETING OFFICER">DIGITAL MARKETING OFFICER</option>
                      <option value="DIRECTOR">DIRECTOR</option>
                      <option value="DRAFTER">DRAFTER</option>
                      <option value="DRIVER">DRIVER</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR">ENVIRONMENT MAINTENANCE INSPECTOR</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR">ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR</option>
                      <option value="ESTATE COORDINATOR">ESTATE COORDINATOR</option>
                      <option value="ESTATE MANAGEMENT ADMINISTRATOR">ESTATE MANAGEMENT ADMINISTRATOR</option>
                      <option value="ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD">ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD</option>
                      <option value="ESTATE MANAGEMENT DEPUTY MANAGER">ESTATE MANAGEMENT DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING DEPUTY MANAGER">FINANCE & ACCOUNTING DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING GENERAL MANAGER">FINANCE & ACCOUNTING GENERAL MANAGER</option>
                      <option value="FINANCE & ACCOUNTING MANAGER">FINANCE & ACCOUNTING MANAGER</option>
                      <option value="FINANCE ADMINISTRATOR">FINANCE ADMINISTRATOR</option>
                      <option value="FINANCE, ACCOUNTING & TAX DEPUTY MANAGER">FINANCE, ACCOUNTING & TAX DEPUTY MANAGER</option>
                      <option value="GENERAL AFFAIR SECTION HEAD">GENERAL AFFAIR SECTION HEAD</option>
                      <option value="GM OPERATIONAL">GM OPERATIONAL</option>
                      <option value="HC & GA DEPUTY GM">HC & GA DEPUTY GM</option>
                      <option value="HC & GA MANAGER">HC & GA MANAGER</option>
                      <option value="HC ADMINISTRATOR">HC ADMINISTRATOR</option>
                      <option value="HC OFFICER">HC OFFICER</option>
                      <option value="HSE INSPECTOR">HSE INSPECTOR</option>
                      <option value="HSE OFFICER">HSE OFFICER</option>
                      <option value="INFRASTRUCTURE INSPECTOR">INFRASTRUCTURE INSPECTOR</option>
                      <option value="INFRASTRUCTURE SITE MANAGER">INFRASTRUCTURE SITE MANAGER</option>
                      <option value="INVOICE ADMINISTRATION">INVOICE ADMINISTRATION</option>
                      <option value="LAND ADMINISTRATOR">LAND ADMINISTRATOR</option>
                      <option value="LAND MANAGER">LAND MANAGER</option>
                      <option value="LAND OFFICER">LAND OFFICER</option>
                      <option value="LAND SECTION HEAD">LAND SECTION HEAD</option>
                      <option value="LAND SURVEYOR">LAND SURVEYOR</option>
                      <option value="LANDSCAPE DESIGN">LANDSCAPE DESIGN</option>
                      <option value="LANDSCAPE INSPECTOR">LANDSCAPE INSPECTOR</option>
                      <option value="LANDSCAPE SECTION HEAD">LANDSCAPE SECTION HEAD</option>
                      <option value="LEGAL & PERMIT MANAGER">LEGAL & PERMIT MANAGER</option>
                      <option value="LEGAL ADMINISTRATOR">LEGAL ADMINISTRATOR</option>
                      <option value="LEGAL OFFICER">LEGAL OFFICER</option>
                      <option value="LEGAL SECTION HEAD">LEGAL SECTION HEAD</option>
                      <option value="LIFE GUARD">LIFE GUARD</option>
                      <option value="MANAGEMENT INFORMATION SYSTEMS MANAGER">MANAGEMENT INFORMATION SYSTEMS MANAGER</option>
                      <option value="MARKETING MANAGER">MARKETING MANAGER</option>
                      <option value="MECHANICAL ELECTRICAL INSPECTOR">MECHANICAL ELECTRICAL INSPECTOR</option>
                      <option value="MECHANICAL ELECTRICAL SECTION HEAD">MECHANICAL ELECTRICAL SECTION HEAD</option>
                      <option value="MECHANICAL ELECTRICAL SITE MANAGER">MECHANICAL ELECTRICAL SITE MANAGER</option>
                      <option value="MESSENGER">MESSENGER</option>
                      <option value="MIS COORDINATOR">MIS COORDINATOR</option>
                      <option value="MIS OFFICER">MIS OFFICER</option>
                      <option value="OFFICE BOY">OFFICE BOY</option>
                      <option value="OPERATOR GENERAL MANAGER">OPERATOR GENERAL MANAGER</option>
                      <option value="OPERATOR HELPER">OPERATOR HELPER</option>
                      <option value="PAYROLL OFFICER">PAYROLL OFFICER</option>
                      <option value="PENGAWAS BANGUNAN">PENGAWAS BANGUNAN</option>
                      <option value="PENGAWAS ME">PENGAWAS ME</option>
                      <option value="PERMIT ADMINISTRATOR">PERMIT ADMINISTRATOR</option>
                      <option value="PERMIT OFFICER">PERMIT OFFICER</option>
                      <option value="PLANNING & DESIGN GENERAL MANAGER">PLANNING & DESIGN GENERAL MANAGER</option>
                      <option value="PLANNING & DESIGN MANAGER">PLANNING & DESIGN MANAGER</option>
                      <option value="PROMOTION ADMINISTRATOR">PROMOTION ADMINISTRATOR</option>
                      <option value="PROMOTION SUPERVISOR">PROMOTION SUPERVISOR</option>
                      <option value="PROMOTION SUPPORT">PROMOTION SUPPORT</option>
                      <option value="PURCHASING ADMINISTRATOR">PURCHASING ADMINISTRATOR</option>
                      <option value="QUALITY CONTROL">QUALITY CONTROL</option>
                      <option value="QUANTITY SURVEYOR ADMINISTRATOR">QUANTITY SURVEYOR ADMINISTRATOR</option>
                      <option value="QUANTITY SURVEYOR ANALYST">QUANTITY SURVEYOR ANALYST</option>
                      <option value="QUANTITY SURVEYOR MANAGER">QUANTITY SURVEYOR MANAGER</option>
                      <option value="QUANTITY SURVEYOR OFFICER">QUANTITY SURVEYOR OFFICER</option>
                      <option value="SALES EXECUTIVE">SALES EXECUTIVE</option>
                      <option value="SALES EXECUTIVE SUPERVISOR">SALES EXECUTIVE SUPERVISOR</option>
                      <option value="SECURITY SECTION HEAD">SECURITY SECTION HEAD</option>
                      <option value="SPORTCLUB, WW & WOW SECTION HEAD">SPORTCLUB, WW & WOW SECTION HEAD</option>
                      <option value="SURVEYOR">SURVEYOR</option>
                      <option value="TAX ADMINISTRATOR">TAX ADMINISTRATOR</option>
                      <option value="TRAINER">TRAINER</option>
                      <option value="WEB DEVELOPER">WEB DEVELOPER</option>
                      <option value="WTP OPERATOR">WTP OPERATOR</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email</label>
                    <input required name="email" type="text" class="form-control border-list-olive">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Gender</label>
                    <select name="jenis_kelamin" class="form-control select2" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Password</label>
                    <input required name="password" type="password" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Confirmation Password</label>
                    <input required name="password2" type="password" class="form-control border-list-olive">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button onclick="return confirm('Apakah User Id yang anda buat sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#imageResult')
          .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
      readURL(input);
    });
  });

  var input = document.getElementById( 'upload' );
  var infoArea = document.getElementById( 'upload-label' );

  input.addEventListener( 'change', showFileName );
  function showFileName( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = 'File name: ' + fileName;
  }
</script>


</body>
</html>
