<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
    <title>Ciputra</title>

    
</head>

<?php
    include 'koneksi.php';
?>

<body class="bg-color-1">
    <div>
        <form class="form" name="login" action="" method="post">
            <div class="align-middle">
                <div class="tengah kotak_tengah2">
                    <center><img id="profile-img" width="200" class="profile-img-card" src="gambar/ciputra.svg" /></center>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <span class="input-group-text warna-icon-login"><i class="fas fa-user"></i></span>
                        </div>
                            <input type="text" name="email" class="form-control input_user border-list" placeholder="Email">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-append">
                            <span class="input-group-text warna-icon-login"><i class="fas fa-key"></i></span>
                        </div>
                            <input type="password" name="password" class="form-control input_pass border-list" placeholder="Password">
                    </div>
                    <a href="#" class="text-decoration">Forgot Password?</a>
                    </br>

                    </br>

                    <table width="100%">
                        <tr>
                            <td style="border: 0; padding: 0px;">
                                <button type="submit" name="submit" class="btn btn-primary button-right">Login</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>

    <?php

    if (isset($_POST['submit'])) {
        $email = $_POST['email'];
        $password = md5(trim($_POST['password']));

        $strSQL = "SELECT email, login_status, password FROM tb_user WHERE email='$email' AND password='$password'";
        $login = mysqli_query($koneksi, $strSQL) or die ("Query Salah!!!");
        $cek = mysqli_fetch_array($login);

        $strSQL3 = "SELECT email, password FROM tb_user WHERE email='$email'";
        $login3 = mysqli_query($koneksi, $strSQL3) or die ("Query Salah!!!");
        $cek3 = mysqli_fetch_array($login3);

        $strSQL2 = "SELECT email, password FROM tb_user WHERE password='$password'";
        $login2 = mysqli_query($koneksi, $strSQL2) or die ("Query Salah!!!");
        $cek2 = mysqli_fetch_array($login2);
        

        if (empty($_POST['password']) && empty($_POST['email'])) {
            echo "<script>alert('Silahkan masukan email & Password!');history.go(-1)</script>";
        }elseif (empty($_POST['email'])) {
            echo "<script>alert('Silahkan masukan email!');history.go(-1)</script>";
        }elseif (empty($_POST['password'])) {
            echo "<script>alert('Silahkan masukan Password!');history.go(-1)</script>";
        }elseif ($email != $cek3['email']) {
            echo "<script>alert('Email yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($password != $cek2['password']) {
            echo "<script>alert('Password yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($cek > 0) {
            session_start();
            if ($cek['login_status'] == "1") {
                $_SESSION['adminlogin'] = $email;
                // $_SESSION['status'] = "login";
                header("location:index.php");
            }elseif ($cek['login_status'] == "2") {
                $_SESSION['userlogin'] = $email;
                // $_SESSION['status'] = "login";
                header("location:User/homeuser.php");
            }    
        }else{
            echo "<script>alert('Email atau Password yang anda masukan salah!');window.location='login.php'; </script>";
        }
    }

    ?>

    <script src="assets/js/jquery.js"></script> 
    <script src="assets/js/popper.js"></script> 
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>