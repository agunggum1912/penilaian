<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);


  $sql2 = "SELECT a.id AS id_karyawan, a.nama_karyawan, a.nik, a.id_user1, a.tgl_masuk, a.status_karyawan, a.lokasi, a.departemen_karyawan, a.posisi_karyawan, a.golongan, b.id, b.nama, c.id_karyawan, c.status_penilaian AS sp1, d.id_karyawan, d.status_penilaian AS sp2, e.id_karyawan, e.status_penilaian AS sp3 FROM tb_karyawan AS a INNER JOIN tb_user AS b ON a.id_user1=b.id INNER JOIN tb_kontrak1 AS c ON a.id=c.id_karyawan INNER JOIN tb_kontrak2 AS d ON a.id=d.id_karyawan INNER JOIN tb_kontrak3 AS e ON a.id=e.id_karyawan ORDER BY nama_karyawan ASC";
  $qry2 = mysqli_query($koneksi, $sql2) or die ("Query karyawan salah!");


  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>View Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">View Employee Assessment</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Name Employee</center></th>
                      <th><center>NIK</center></th>
                      <th><center>Status Contract</center></th>
                      <th><center>Status Assessment</center></th>
                      <th><center>User Process</center></th>
                      <th><center>Date Join</center></th>
                      <th><center>Location Employment</center></th>
                      <th><center>Departement</center></th>
                      <th><center>Position</center></th>
                      <th><center>Golongan</center></th>
                      <th colspan="2"><center>Action</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no = 0; 
                      while ($row2 = mysqli_fetch_array($qry2)) {
                        $no++;
                        $nama_karyawan = $row2['nama_karyawan'];
                    ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row2['nama_karyawan'];?></td>
                      <td><?php echo $row2['nik'];?></td>
                      <td>
                        <?php if ($row2['sp3'] > 0) {echo "Contract 3";}elseif($row2['sp2'] > 0){echo "Contract 2";}elseif($row2['sp1'] > 0){echo "Contract 1";}elseif($row2['sp1'] == ""){echo "Contract 1";}?>
                      </td>
                      <td>
                        <?php if ($row2['sp3'] > 0) {echo "Assessment ".$row2['sp3'];}elseif($row2['sp2'] > 0){echo "Assessment ".$row2['sp2'];}elseif($row2['sp1'] > 0){echo "Assessment ".$row2['sp1'];}elseif($row2['sp1'] == ""){echo "On Process User";}?>
                      </td>
                      <td><?php echo $row2['nama'];?></td>
                      <td><?php echo date("d-m-Y", strtotime($row2['tgl_masuk']));?></td>
                      <td><?php echo $row2['lokasi'];?></td>
                      <td><?php echo $row2['departemen_karyawan'];?></td>
                      <td><?php echo $row2['posisi_karyawan'];?></td>
                      <td><?php echo $row2['golongan'];?></td>
                      <td width="15%"><a href="editass.php?nik=<?php echo $row2['nik'];?>" class="btn btn-info float-right button-space"><i class='font-setting-4 fas fa-edit'></i></a></td>
                      <td><a href="delete-karyawan.php?id_karyawan=<?php echo $row2['id_karyawan'];?>" onclick="return confirm('Anda yakin ingin menghapus Employee <?php echo $nama_karyawan;?>')" class="btn btn-danger float-right button-space"><i class='font-setting-4 fas fa-trash-alt'></i></a></td>
                    </tr>
                    <?php };?>
                  </tbody>
                </table>
                <b>*Lihat paling kanan untuk proses Edit/Delete/Print</b>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
