<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
	<title>Ciputra</title>

	<style type="text/css">
		body{
			font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
			-webkit-print-color-adjust: exact;
			padding: 80px 40px;
		}

		.logo{
			height: 50px;
			width: 75px;
		}

		.namalogo {
			position: absolute;
			margin-top: 10px;
			padding-left: 20px;
		}

		.namalogo2 {
			padding-left: 20px;
			text-align: center;
			font-size: 18pt;
			font-weight: bold;
		}

		h3 {
			margin: 0;
		}

		.garis {
			border-top: 2px solid #000;
			padding-top: 10px;
			padding-bottom: 20px; 
		}

		.X {
			border:1px solid #000;
			padding: 0px 8px;
			margin-left: 20px;
			margin-right: 20px;
		}

		.X2 {
			border:1px solid #000;
			padding: 0px 8px;
			margin-right: 40px;
		}

	</style>


	<?php
	include 'koneksi.php';

	// mengaktifkan session
	session_start();
	if (!isset($_SESSION['adminlogin'])) {
	// if($_SESSION['status'] != "login") {
		echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="logout.php"</script>';
	}

	$sql = "SELECT id, nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
	$qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
	$row = mysqli_fetch_array($qry);
	$iduser = $row['id'];

	if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$data = explode(".", $id);

		$data1 = $data[0];
		$data2 = $data[1];
		$data3 = $data[2];

		$sql2 = "SELECT a.id AS id2, a.nama_karyawan, a.nik, a.id_user1, a.departemen_karyawan, a.posisi_karyawan, a.lokasi, a.golongan, a.tgl_masuk, b.id, b.email, b.nama, c.id_karyawan, c.status_penilaian AS sp1, c.mulai_kontrak AS mk1, c.selesai_kontrak AS sk1, c.banyak_penilaian AS bp1, c.id_proses, d.id_karyawan, d.status_penilaian AS sp2, d.mulai_kontrak AS mk2, d.selesai_kontrak AS sk2, d.banyak_penilaian AS bp2, d.id_proses, e.id_karyawan, e.status_penilaian AS sp3, e.mulai_kontrak AS mk3, e.selesai_kontrak AS sk3, e.banyak_penilaian AS bp3, e.id_proses, f.id_karyawan, f.mulai_penilaian AS mp1p1, f.selesai_penilaian AS sp1p1, f.hasil AS hk1p1, g.id_karyawan, g.mulai_penilaian AS mp1p2, g.selesai_penilaian AS sp1p2, g.hasil AS hk1p2, h.id_karyawan, h.mulai_penilaian AS mp1p3, h.selesai_penilaian AS sp1p3, h.hasil AS hk1p3, i.id_karyawan, i.mulai_penilaian AS mp1p4, i.selesai_penilaian AS sp1p4, i.hasil AS hk1p4, j.id_karyawan, j.mulai_penilaian AS mp2p1, j.selesai_penilaian AS sp2p1, j.hasil AS hk2p1, k.id_karyawan, k.mulai_penilaian AS mp2p2, k.selesai_penilaian AS sp2p2, k.hasil AS hk2p2, l.id_karyawan, l.mulai_penilaian AS mp2p3, l.selesai_penilaian AS sp2p3, l.hasil AS hk2p3, m.id_karyawan, m.mulai_penilaian AS mp2p4, m.selesai_penilaian AS sp2p4, m.hasil AS hk2p4, n.id_karyawan, n.mulai_penilaian AS mp3p1, n.selesai_penilaian AS sp3p1, n.hasil AS hk3p1, o.id_karyawan, o.mulai_penilaian AS mp3p2, o.selesai_penilaian AS sp3p2, o.hasil AS hk3p2, p.id_karyawan, p.mulai_penilaian AS mp3p3, p.selesai_penilaian AS sp3p3, p.hasil AS hk3p3, q.id_karyawan, q.mulai_penilaian AS mp3p4, q.selesai_penilaian AS sp3p4, q.hasil AS hk3p4 FROM tb_karyawan AS a INNER JOIN tb_user AS b ON a.id_user1=b.id INNER JOIN tb_kontrak1 AS c ON a.id=c.id_karyawan INNER JOIN tb_kontrak2 AS d ON a.id=d.id_karyawan INNER JOIN tb_kontrak3 AS e ON a.id=e.id_karyawan INNER JOIN tb_kon1_pen1 AS f ON a.id=f.id_karyawan INNER JOIN tb_kon1_pen2 AS g ON a.id=g.id_karyawan INNER JOIN tb_kon1_pen3 AS h ON a.id=h.id_karyawan INNER JOIN tb_kon1_pen4 AS i ON a.id=i.id_karyawan INNER JOIN tb_kon2_pen1 AS j ON a.id=j.id_karyawan INNER JOIN tb_kon2_pen2 AS k ON a.id=k.id_karyawan INNER JOIN tb_kon2_pen3 AS l ON a.id=l.id_karyawan INNER JOIN tb_kon2_pen4 AS m ON a.id=m.id_karyawan INNER JOIN tb_kon3_pen1 AS n ON a.id=n.id_karyawan INNER JOIN tb_kon3_pen2 AS o ON a.id=o.id_karyawan INNER JOIN tb_kon3_pen3 AS p ON a.id=p.id_karyawan INNER JOIN tb_kon3_pen4 AS q ON a.id=q.id_karyawan WHERE a.id=$data1";

		$qry2 = mysqli_query($koneksi, $sql2) or die ("Query karyawan salah!");
		$row2 = mysqli_fetch_array($qry2);

		if ($data2 == "1" && $data3 == "1") {
			if ($row2['sp1'] == 1 && $row2['bp1'] == 1) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon1_pen1 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp1p1'];
				$kontrak1 = "X";
				$kontrak2 = "_";
				$kontrak3 = "_";
				$mk1 = $row2['mk1'];
				$sk1 = $row2['sk1'];
				$mk2 = "";
				$sk2 = "";
				$mk3 = "";
				$sk3 = "";
				$mp1 = date("d-m-Y", strtotime($row2['mp1p1']));
				$sp1 = date("d-m-Y", strtotime($row2['sp1p1']));
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "X";
				$p2 = "_";
				$p3 = "_";
				$p4 = "_";
			}
		}elseif ($data2 == "1" && $data3 == "2") {
			if ($row2['sp1'] == 2 && $row2['bp1'] == 2) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon1_pen2 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp1p2'];
				$kontrak1 = "X";
				$kontrak2 = "_";
				$kontrak3 = "_";
				$mk1 = $row2['mk1'];
				$sk1 = $row2['sk1'];
				$mk2 = "";
				$sk2 = "";
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = date("d-m-Y", strtotime($row2['mp1p2']));
				$sp2 = date("d-m-Y", strtotime($row2['sp1p2']));
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "X";
				$p3 = "_";
				$p4 = "_";	
			}
		}elseif ($data2 == "1" && $data3 == "3") {
			if ($row2['sp1'] == 3 && $row2['bp1'] == 3) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon1_pen3 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp1p3'];
				$kontrak1 = "X";
				$kontrak2 = "_";
				$kontrak3 = "_";
				$mk1 = $row2['mk1'];
				$sk1 = $row2['sk1'];
				$mk2 = "";
				$sk2 = "";
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = date("d-m-Y", strtotime($row2['mp1p3']));
				$sp3 = date("d-m-Y", strtotime($row2['sp1p3']));
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "_";
				$p3 = "X";
				$p4 = "_";
			}
		}elseif ($data2 == "1" && $data3 == "4") {
			if ($row2['sp1'] == 4 && $row2['bp1'] == 4) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon1_pen4 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp1p4'];
				$kontrak1 = "X";
				$kontrak2 = "_";
				$kontrak3 = "_";
				$mk1 = $row2['mk1'];
				$sk1 = $row2['sk1'];
				$mk2 = "";
				$sk2 = "";
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = date("d-m-Y", strtotime($row2['mp1p4']));
				$sp4 = date("d-m-Y", strtotime($row2['sp1p4']));
				$p1 = "_";
				$p2 = "_";
				$p3 = "_";
				$p4 = "X";
			}
		}elseif ($data2 == "2" && $data3 == "1") {
			if ($row2['sp2'] == 1 && $row2['bp2'] == 1) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon2_pen1 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp2p1'];
				$kontrak1 = "_";
				$kontrak2 = "X";
				$kontrak3 = "_";
				$mk1 = "";
				$sk1 = "";
				$mk2 = $row2['mk2'];
				$sk2 = $row2['sk2'];
				$mk3 = "";
				$sk3 = "";
				$mp1 = date("d-m-Y", strtotime($row2['mp2p1']));
				$sp1 = date("d-m-Y", strtotime($row2['sp2p1']));
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "X";
				$p2 = "_";
				$p3 = "_";
				$p4 = "_";
			}
		}elseif ($data2 == "2" && $data3 == "2") {
			if ($row2['sp2'] == 2 && $row2['bp2'] == 2) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon2_pen2 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp2p2'];
				$kontrak1 = "_";
				$kontrak2 = "X";
				$kontrak3 = "_";
				$mk1 = "";
				$sk1 = "";
				$mk2 = $row2['mk2'];
				$sk2 = $row2['sk2'];
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = date("d-m-Y", strtotime($row2['mp2p2']));
				$sp2 = date("d-m-Y", strtotime($row2['sp2p2']));
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "X";
				$p3 = "_";
				$p4 = "_";
			}
		}elseif ($data2 == "2" && $data3 == "3") {
			if ($row2['sp2'] == 3 && $row2['bp2'] == 3) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon2_pen3 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp2p3'];
				$kontrak1 = "_";
				$kontrak2 = "X";
				$kontrak3 = "_";
				$mk1 = "";
				$sk1 = "";
				$mk2 = $row2['mk2'];
				$sk2 = $row2['sk2'];
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = date("d-m-Y", strtotime($row2['mp2p3']));
				$sp3 = date("d-m-Y", strtotime($row2['sp2p3']));
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "_";
				$p3 = "X";
				$p4 = "_";
			}
		}elseif ($data2 == "2" && $data3 == "4") {
			if ($row2['sp2'] == 4 && $row2['bp2'] == 4) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon2_pen4 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp2p4'];
				$kontrak1 = "_";
				$kontrak2 = "X";
				$kontrak3 = "_";
				$mk1 = "";
				$sk1 = "";
				$mk2 = $row2['mk2'];
				$sk2 = $row2['sk2'];
				$mk3 = "";
				$sk3 = "";
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = date("d-m-Y", strtotime($row2['mp2p4']));
				$sp4 = date("d-m-Y", strtotime($row2['sp2p4']));
				$p1 = "_";
				$p2 = "_";
				$p3 = "_";
				$p4 = "X";
			}
		}elseif ($data2 == "3" && $data3 == "1") {
			if ($row2['sp3'] == 1 && $row2['bp3'] == 1) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon3_pen1 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp3p1'];
				$kontrak1 = "_";
				$kontrak2 = "_";
				$kontrak3 = "X";
				$mk1 = "";
				$sk1 = "";
				$mk2 = $row2['mk2'];
				$sk2 = $row2['sk2'];
				$mk3 = "";
				$sk3 = "";
				$mp1 = date("d-m-Y", strtotime($row2['mp3p1']));
				$sp1 = date("d-m-Y", strtotime($row2['sp3p1']));
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "X";
				$p2 = "_";
				$p3 = "_";
				$p4 = "_";
			}
		}elseif ($data2 == "3" && $data3 == "2") {
			if ($row2['sp3'] == 2 && $row2['bp3'] == 2) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon3_pen2 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp3p2'];
				$kontrak1 = "_";
				$kontrak2 = "_";
				$kontrak3 = "X";
				$mk1 = "";
				$sk1 = "";
				$mk2 = "";
				$sk2 = "";
				$mk3 = $row2['mk3'];
				$sk3 = $row2['sk3'];
				$mp1 = "";
				$sp1 = "";
				$mp2 = date("d-m-Y", strtotime($row2['mp3p2']));
				$sp2 = date("d-m-Y", strtotime($row2['sp3p2']));
				$mp3 = "";
				$sp3 = "";
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "X";
				$p3 = "_";
				$p4 = "_";
			}
		}elseif ($data2 == "3" && $data3 == "3") {
			if ($row2['sp3'] == 3 && $row2['bp3'] == 3) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon3_pen3 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp3p3'];
				$kontrak1 = "_";
				$kontrak2 = "_";
				$kontrak3 = "X";
				$mk1 = "";
				$sk1 = "";
				$mk2 = "";
				$sk2 = "";
				$mk3 = $row2['mk3'];
				$sk3 = $row2['sk3'];
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = date("d-m-Y", strtotime($row2['mp3p3']));
				$sp3 = date("d-m-Y", strtotime($row2['sp3p3']));
				$mp4 = "";
				$sp4 = "";
				$p1 = "_";
				$p2 = "_";
				$p3 = "X";
				$p4 = "_";
			}
		}elseif ($data2 == "3" && $data3 == "4") {
			if ($row2['sp3'] == 4 && $row2['bp3'] == 4) {
				echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
			}else{
				$sql3 = "SELECT * FROM tb_kon3_pen4 INNER JOIN tb_karyawan ON id_karyawan=id WHERE id_karyawan=$data1";
				$qry3 = mysqli_query($koneksi, $sql3);
				$row3 = mysqli_fetch_array($qry3);
				$tgl = $row2['mp3p4'];
				$kontrak1 = "_";
				$kontrak2 = "_";
				$kontrak3 = "X";
				$mk1 = "";
				$sk1 = "";
				$mk2 = "";
				$sk2 = "";
				$mk3 = $row2['mk3'];
				$sk3 = $row2['sk3'];
				$mp1 = "";
				$sp1 = "";
				$mp2 = "";
				$sp2 = "";
				$mp3 = "";
				$sp3 = "";
				$mp4 = date("d-m-Y", strtotime($row2['mp3p4']));
				$sp4 = date("d-m-Y", strtotime($row2['sp3p4']));
				$p1 = "_";
				$p2 = "_";
				$p3 = "_";
				$p4 = "X";
			}
		}
	}
	?>



</head>
<body>
	<div style="page-break-after:always;">
		<img src="gambar/logociputra.svg" class="logo">
		<span class="namalogo">Ciputra Group</span>

		<h3><center>PENILAIAN PRESENTASI KARYAWAN DALAM MASA <br> PERJANJIAN KERJA</center></h3>
		<span><center>(Untuk dikirimkan kembali kepada departemen Sumber Daya Manusia)</center></span>
		<br>
		<table border="0">
			<tr>
				<td width="100px">Kepada Yth</td>
				<td>:</td>
				<td>Bpk/Ibu. <?php echo $row2['nama'];?></td>
			</tr>
			<tr>
				<td width="100px">Dari</td>
				<td>:</td>
				<td>Departemen Sumber Daya Manusia</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td><?php echo date("d-m-Y", strtotime($tgl));?></td>
			</tr>
		</table>

		<br>
		<div class="garis">
			<span>Sehubungan dengan kegiatan evaluasi periodik 3 bulanan terhadap karyawan yang masih dalam status perjanjian kerja, maka dengan ini kami sampaikan lembar isian penilaian presentasi dari:</span>
		</div>

		<table border="0">
			<tr>
				<td width="150px">Nama Karyawan</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['nama_karyawan'];?></td>
				<td width="100px">NIK</td>
				<td>:</td>
				<td><?php echo $row2['nik'];?></td>
			</tr>
			<tr>
				<td width="150px">Departemen</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['departemen_karyawan'];?></td>
				<td width="100px">Golongan</td>
				<td>:</td>
				<td><?php echo $row2['golongan'];?></td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['posisi_karyawan'];?></td>
				<td width="100px">Tgl Masuk</td>
				<td>:</td>
				<td><?php echo date("d-m-Y", strtotime($row2['tgl_masuk']));?></td>
			</tr>
			<tr>
				<td width="150px">Lokasi</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['lokasi'];?></td>
			</tr>
		</table>
		<table border="0">
			<tr>
				<td width="150px">Status Kepegawaian</td>
				<td>:</td>
				<td><span class="X"><?php echo $kontrak1;?></span></td>
				<td width="100px">Kontrak 1</td>
				<td>tanggal</td>
				<td width="120px"><center><u><?php echo $mk1;?></u></center></td>
				<td>s/d</td>
				<td width="120px"><center><u><?php echo $sk1;?></u></center></td>
			</tr>
			<tr>
				<td width="150px"></td>
				<td></td>
				<td><span class="X"><?php echo $kontrak2;?></span></td>
				<td width="100px">Kontrak 2</td>
				<td>tanggal</td>
				<td><center><u><?php echo $mk2;?></u></center></td>
				<td>s/d</td>
				<td><center><u><?php echo $sk2;?></u></center></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><span class="X"><?php echo $kontrak3;?></span></td>
				<td width="100px">Kontrak 3</td>
				<td>tanggal</td>
				<td><center><u><?php echo $mk3;?></u></center></td>
				<td>s/d</td>
				<td><center><u><?php echo $sk3;?></u></center></td>
			</tr>
		</table>
	</br>
	
	<span>Periode Penilaian:</span>
	<table border="0">
		<tr>
			<td><span class="X2"><?php echo $p1;?></span></td>
			<td>3 bulan ke-1: tanggal</td>
			<td width="120px"><center><u><?php echo $mp1;?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php echo $sp1;?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php echo $p2;?></span></td>
			<td>3 bulan ke-2: tanggal</td>
			<td width="120px"><center><u><?php echo $mp2;?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php echo $sp2;?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php echo $p3;?></span></td>
			<td>3 bulan ke-3: tanggal</td>
			<td width="120px"><center><u><?php echo $mp3;?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php echo $sp3;?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php echo $p4;?></span></td>
			<td>3 bulan ke-4: tanggal</td>
			<td width="120px"><center><u><?php echo $mp4;?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php echo $sp4;?></u></center></td>
		</tr>
	</table>
	<br>

	<span>Mohon Bapak/Ibu memberikan penilaian untuk periode penilaian yang sedang</span>
	<br>
	<br>
	<span>Terimakasih,</span>
	<br>
	<br>
	<span><b>&emsp;DAHLIANA</b></span>
	<br>
	<br>
	<span>Departemen SDM</span>
	<br>
	<p style="font-size: 12px; line-height: 14px;">
		Jl. Prof DR. Satrio kav.6<br>
		Jakarta 12940, INDONESIA<br>
		Tel: (62-21) 5225858,5226868,5207333<br>
		Fax: (62-21) 5274125, 5205262,5205886
	</p>
</div>
<div style="page-break-after:always;">
	<br>
	<br>
	<br>
	<br>

	<span style="font-size: 8pt;">LEMBAR PENILAIAN - akan terisi otomatis checkmark ( &radic; ) sesuai penilaian di sistem</span>
	<span style="font-size: 8pt; padding-left: 150px;">Golongan I-IV</span>

	<table border="1" style="border-collapse: collapse;">
		<tr>
			<td rowspan="3" width="230px"><center>ASPEK</center></td>
			<td colspan="5"><center>PENILAIAN</center></td>
		</tr>
		<tr>
			<td width="80px">RENDAH</td>
			<td width="80px">KURANG</td>
			<td width="80px">CUKUP</td>
			<td width="80px">BAIK</td>
			<td width="80px">TINGGI</td>
		</tr>
		<tr style="font-size: 8pt; line-height: 10px;">
			<td style="padding-top: 5px; padding-left: 5px;">Tidak Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Sebagian Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Melampaui Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Jauh Melampaui Persyaratan</td>
		</tr>
		<tr>
			<td colspan="6">A. KEHADIRAN</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Ketepatan <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Hadir pada waktu yang sudah ditetapkan
			</td>
			<td><center><?php if($row3['jawaban1'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban1'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban1'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban1'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban1'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Absensi <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Ketidakhadiran disebabkan oleh ijin atau alpha
			</td>
			<td><center><?php if($row3['jawaban2'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban2'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban2'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban2'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban2'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">B. PENGETAHUAN</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Pengetahuan Teoritis <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Pengetahuan teoritis tentang pekerjaan, yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				didapat dari pendidikan formal dan / informal
			</td>
			<td><center><?php if($row3['jawaban3'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban3'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban3'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban3'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban3'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Kemampuan menetapkan pengetahuan teoritis <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				dalam pelaksanaan pekerjaan, Kemampuan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerja praktis terapan
			</td>
			<td><center><?php if($row3['jawaban4'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban4'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban4'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban4'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban4'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				3. Kemampuan Mempelajari Hal Baru <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kemampuan untuk beradaptasi, mempelajari <br> &nbsp;&nbsp;&nbsp;&nbsp;
				dan melaksanakan hal-hal baru dalam pekerjaan, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				motivasi untuk menemukan hal baru
			</td>
			<td><center><?php if($row3['jawaban5'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban5'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban5'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban5'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban5'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">C. HASIL KERJA</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Kualitas Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Keterampilan, ketelitian, kecermatan dan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerapihan hasil kerja, termasuk tingkat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				pengawasan dan penelitian ulang yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diperlukan
			</td>
			<td><center><?php if($row3['jawaban6'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban6'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban6'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban6'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban6'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Kuantitas Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Jumlah / Volume kerja (output) yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diselesaikan dalam jangka waktu tertentu, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				termasuk kecepatan kera dan tingkat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				pengawasan yang diperlukan
			</td>
			<td><center><?php if($row3['jawaban7'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban7'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban7'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban7'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban7'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">C. SIKAP KERJA</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Cara Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kemauan untuk bekerja secara sistematis dan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				efisien
			</td>
			<td><center><?php if($row3['jawaban8'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban8'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban8'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban8'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban8'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Komitmen <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Rasa tanggung jawab terhadap pekerjaan, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				tingkat pengutamaan kepentingan pekerjaan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diatas kepentingan lainnya
			</td>
			<td><center><?php if($row3['jawaban9'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban9'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban9'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban9'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban9'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				3. Kerja Sama <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kesediaan bekerja sama dan membantu <br> &nbsp;&nbsp;&nbsp;&nbsp;
				memberi dan menerima masukan ke/dari <br> &nbsp;&nbsp;&nbsp;&nbsp;
				atasan/rekan kerja/bawahan/orang lain
			</td>
			<td><center><?php if($row3['jawaban10'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban10'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban10'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban10'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban10'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				4. Keselamatan Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kesediaan untuk mementingkan keselamatan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerja diri sendiri maupun orang lian, merawat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				dan menggunakan peralatan kantor secara <br> &nbsp;&nbsp;&nbsp;&nbsp;
				efesien
			</td>
			<td><center><?php if($row3['jawaban11'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban11'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban11'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban11'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row3['jawaban11'] == 5) echo "&radic;";?></center></td>
		</tr>
	</table>
</div>
<div style="page-break-after:always;">
	<br>
	<br>
	<br>
	<br>

	<table border="1" style="display: block; font-size: 10pt; border-collapse: collapse;">
		<tr>
			<td align="left" valign="top" height="200" width="700"><b>Keterangan tentang prestasi atau unjuk kerja:</b><br>
			<?php echo $row3['ket1'] ?></td>
		</tr>
		<tr>
			<td align="left" valign="top" height="200"><b>Hal-hal penting yang terjadi:</b><br>
			<?php echo $row3['ket2'] ?></td>
		</tr>
	</table>
	<br>
	<br>
	<br>


	<span>Tanggal Penilaian:<u><?php echo date("d-m-Y", strtotime($row3['tgl_penilaian']));?></u></span>
	<br>
	<br>
	<br>

	<span><b>KESIMPULAN AKHIR: <?php echo $row3['hasil'];?></b></span><br>
	<span style="padding-left: 30px;">A = Memuaskan, jauh diatas rata-rata <br></span>
	<span style="padding-left: 30px;">B = Baik, diatas rata-rata <br></span>
	<span style="padding-left: 30px;">C = Cukup, tergolong rata-rata <br></span>
	<span style="padding-left: 30px;">D = Kurang memuaskan, dibawah rata-rata <br></span>
	<span style="padding-left: 30px;">E = Tidak memuaskan, jauh dibawah rata-rata</span>
	<br>
	<br>
	<br>
	<br>

	<table border="0">
		<tr>
			<td width="500" height="100" align="left" valign="top">Atasan,</td>
			<td align="left" valign="top">&nbsp;Mengetahui,</td>
		</tr>
		<tr>
			<td><?php echo $row2['nama'];?></td>
			<td>Manager SDM</td>
		</tr>
	</table>

</div>


<script>
	window.print();
</script>


</body>
</html>