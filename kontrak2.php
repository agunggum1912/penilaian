<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  $id = $_GET['id_karyawan'];
  $sql2 ="SELECT a.id_karyawan, a.status_penilaian, a.mulai_kontrak, a.selesai_kontrak, a.banyak_penilaian, b.id, b.nik, b.status_karyawan, c.id_karyawan, c.mulai_penilaian AS mulai_nilai1, c.selesai_penilaian AS selesai_nilai1, d.id_karyawan, d.mulai_penilaian AS mulai_nilai2, d.selesai_penilaian AS selesai_nilai2, e.id_karyawan, e.mulai_penilaian AS mulai_nilai3, e.selesai_penilaian AS selesai_nilai3, f.id_karyawan, f.mulai_penilaian AS mulai_nilai4, f.selesai_penilaian AS selesai_nilai4 FROM tb_kontrak2 AS a INNER JOIN tb_karyawan AS b ON a.id_karyawan=b.id INNER JOIN tb_kon2_pen1 AS c ON a.id_karyawan=c.id_karyawan INNER JOIN tb_kon2_pen2 AS d ON a.id_karyawan=d.id_karyawan INNER JOIN tb_kon2_pen3 AS e ON a.id_karyawan=e.id_karyawan INNER JOIN tb_kon2_pen4 AS f ON a.id_karyawan=f.id_karyawan WHERE a.id_karyawan='$id'";
  $qry2 = mysqli_query($koneksi, $sql2) or die ("Query salah!");
  $row2 = mysqli_fetch_array($qry2);
  $nik = $row2['nik'];


  if (isset($_POST['submit'])) {
    $id2 = $_POST['id'];
    $mulai_kontrak = trim($_POST['mulai_kontrak']);
    $selesai_kontrak = trim($_POST['selesai_kontrak']);
    $status_karyawan = trim($_POST['status_karyawan']);
    $banyak_penilaian = trim($_POST['banyak_penilaian']);
    $mp1 = trim($_POST['mp1']);
    $sp1 = trim($_POST['sp1']);
    $mp2 = trim($_POST['mp2']);
    $sp2 = trim($_POST['sp2']);
    $mp3 = trim($_POST['mp3']);
    $sp3 = trim($_POST['sp3']);
    $mp4 = trim($_POST['mp4']);
    $sp4 = trim($_POST['sp4']);

    // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    function ubahMK($mulai_kontrak){
    $pisahmk = explode('/',$mulai_kontrak);
    $arraymk = array($pisahmk[2],$pisahmk[1],$pisahmk[0]);
    $satukanmk = implode('-',$arraymk);
     return $satukanmk;
    }

    $mulai_kontrak2 = ubahMK($mulai_kontrak);


    function ubahSK($selesai_kontrak){
    $pisahsk = explode('/',$selesai_kontrak);
    $arraysk = array($pisahsk[2],$pisahsk[1],$pisahsk[0]);
    $satukansk = implode('-',$arraysk);
     return $satukansk;
    }

    $selesai_kontrak2 = ubahSK($selesai_kontrak);

    
    if (!empty($mp1)) {
      function ubahmp1($mp1){
      $pisahmp1 = explode('/',$mp1);
      $arraymp1 = array($pisahmp1[2],$pisahmp1[1],$pisahmp1[0]);
      $satukanmp1 = implode('-',$arraymp1);
       return $satukanmp1;
      }
      
      $mp1_2 = ubahmp1($mp1);
    }


    if (!empty($sp1)) {
      function ubahsp1($sp1){
      $pisahsp1 = explode('/',$sp1);
      $arraysp1 = array($pisahsp1[2],$pisahsp1[1],$pisahsp1[0]);
      $satukansp1 = implode('-',$arraysp1);
       return $satukansp1;
      }

      $sp1_2 = ubahsp1($sp1);

    }

    
    if (!empty($mp2)) {
      function ubahmp2($mp2){
      $pisahmp2 = explode('/',$mp2);
      $arraymp2 = array($pisahmp2[2],$pisahmp2[1],$pisahmp2[0]);
      $satukanmp2 = implode('-',$arraymp2);
       return $satukanmp2;
      }

      $mp2_2 = ubahmp2($mp2);
    }

    
    if (!empty($sp2)) {
      function ubahsp2($sp2){
      $pisahsp2 = explode('/',$sp2);
      $arraysp2 = array($pisahsp2[2],$pisahsp2[1],$pisahsp2[0]);
      $satukansp2 = implode('-',$arraysp2);
       return $satukansp2;
      }

      $sp2_2 = ubahsp2($sp2);
    }

    
    if (!empty($mp3)) {
      function ubahmp3($mp3){
      $pisahmp3 = explode('/',$mp3);
      $arraymp3 = array($pisahmp3[2],$pisahmp3[1],$pisahmp3[0]);
      $satukanmp3 = implode('-',$arraymp3);
       return $satukanmp3;
      }

      $mp3_2 = ubahmp3($mp3);
    }

    
    if (!empty($sp3)) {
      function ubahsp3($sp3){
      $pisahsp3 = explode('/',$sp3);
      $arraysp3 = array($pisahsp3[2],$pisahsp3[1],$pisahsp3[0]);
      $satukansp3 = implode('-',$arraysp3);
       return $satukansp3;
      }

      $sp3_2 = ubahsp3($sp3);
    }

    
    if (!empty($mp4)) {
      function ubahmp4($mp4){
      $pisahmp4 = explode('/',$mp4);
      $arraymp4 = array($pisahmp4[2],$pisahmp4[1],$pisahmp4[0]);
      $satukanmp4 = implode('-',$arraymp4);
       return $satukanmp4;
      }

      $mp4_2 = ubahmp4($mp4);
    }

    
    if (!empty($sp4)) {
      function ubahsp4($sp4){
      $pisahsp4 = explode('/',$sp4);
      $arraysp4 = array($pisahsp4[2],$pisahsp4[1],$pisahsp4[0]);
      $satukansp4 = implode('-',$arraysp4);
       return $satukansp4;
      }

      $sp4_2 = ubahsp4($sp4);
    }


    
    if (empty($mulai_kontrak)) {
      echo "<script>alert('Start Contract harus di isi!');history.go(-1)</script>";
    }elseif (empty($selesai_kontrak)) {
      echo "<script>alert('Finish Contract Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($status_karyawan)) {
      echo "<script>alert('Assessment Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($banyak_penilaian)) {
      echo "<script>alert('Sum Assessment harus di isi!');history.go(-1)</script>";  
    }elseif($status_karyawan == "1"){
      if (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 telah terisi, silahkan hapus atau ganti Assessment Status jadi 2!');history.go(-1)</script>";
      }elseif (!empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 telah terisi, silahkan hapus atau ganti Assessment Status jadi 2!');history.go(-1)</script>";
      }else{
        $sqlup1k1 = "UPDATE tb_kontrak2 SET status_penilaian='$status_karyawan', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id2'";
        $qryup1k1 = mysqli_query($koneksi, $sqlup1k1) or die("Query sqlup1k1 salah!");

        $sqlup1p1 = "UPDATE tb_kon2_pen1 SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id_karyawan='$id2'";
        $qryup1p1 = mysqli_query($koneksi, $sqlup1p1);

        $sqlup1p2 = "UPDATE tb_kon2_pen2 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup1p2 = mysqli_query($koneksi, $sqlup1p2);

        $sqlup1p3 = "UPDATE tb_kon2_pen3 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup1p3 = mysqli_query($koneksi, $sqlup1p3);

        $sqlup1p4 = "UPDATE tb_kon2_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup1p4 = mysqli_query($koneksi, $sqlup1p4);

        if ($row2['status_karyawan'] != "Kontrak 3") {
          $sqlkaryawan4 = "UPDATE tb_karyawan SET status_karyawan='Kontrak 2' WHERE id='$id2'";
          $qrykaryawan4 = mysqli_query($koneksi, $sqlkaryawan4);
        }

        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "2") {
      if ($banyak_penilaian == "1" && $status_karyawan == "2") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }else{
        $sqlup2k1 = "UPDATE tb_kontrak2 SET banyak_penilaian='$banyak_penilaian', status_penilaian='$status_karyawan', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id2'";
        $qryup2k1 = mysqli_query($koneksi, $sqlup2k1);

        $sqlup2p1 = "UPDATE tb_kon2_pen1 SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id_karyawan='$id2'";
        $qryup2p1 = mysqli_query($koneksi, $sqlup2p1);

        $sqlup2p2 = "UPDATE tb_kon2_pen2 SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id_karyawan='$id2'";
        $qryup2p2 = mysqli_query($koneksi, $sqlup2p2);

        $sqlup2p3 = "UPDATE tb_kon2_pen3 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup2p3 = mysqli_query($koneksi, $sqlup2p3);

        $sqlup2p4 = "UPDATE tb_kon2_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup2p4 = mysqli_query($koneksi, $sqlup2p4);

        if ($row2['status_karyawan'] != "Kontrak 3") {
          $sqlkaryawan4 = "UPDATE tb_karyawan SET status_karyawan='Kontrak 2' WHERE id='$id2'";
          $qrykaryawan4 = mysqli_query($koneksi, $sqlkaryawan4);
        }

        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "3") {
      if ($banyak_penilaian == "1" && $status_karyawan == "3") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "2" && $status_karyawan == "3") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }else{
        $sqlup3k1 = "UPDATE tb_kontrak2 SET banyak_penilaian='$banyak_penilaian', status_penilaian='$status_karyawan', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id2'";
        $qryup3k1 = mysqli_query($koneksi, $sqlup3k1);

        $sqlup3p1 = "UPDATE tb_kon2_pen1 SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id_karyawan='$id2'";
        $qryup3p1 = mysqli_query($koneksi, $sqlup3p1);

        $sqlup3p2 = "UPDATE tb_kon2_pen2 SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id_karyawan='$id2'";
        $qryup3p2 = mysqli_query($koneksi, $sqlup3p2);

        $sqlup3p3 = "UPDATE tb_kon2_pen3 SET mulai_penilaian='$mp3_2', selesai_penilaian='$sp3_2' WHERE id_karyawan='$id2'";
        $qryup3p3 = mysqli_query($koneksi, $sqlup3p3);

        $sqlup3p4 = "UPDATE tb_kon2_pen4 SET mulai_penilaian='', selesai_penilaian='' WHERE id_karyawan='$id2'";
        $qryup3p4 = mysqli_query($koneksi, $sqlup3p4);

        if ($row2['status_karyawan'] != "Kontrak 3") {
          $sqlkaryawan4 = "UPDATE tb_karyawan SET status_karyawan='Kontrak 2' WHERE id='$id2'";
          $qrykaryawan4 = mysqli_query($koneksi, $sqlkaryawan4);
        }

        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "4") {
      if ($banyak_penilaian == "1" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "2" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "3" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }else{
        $sqlup4k1 = "UPDATE tb_kontrak2 SET banyak_penilaian='$banyak_penilaian' status_penilaian='$status_karyawan', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id_karyawan='$id2'";
        $qryup4k1 = mysqli_query($koneksi, $sqlup4k1);

        $sqlup4p1 = "UPDATE tb_kon2_pen1 SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id_karyawan='$id2'";
        $qryup4p1 = mysqli_query($koneksi, $sqlup4p1);

        $sqlup4p2 = "UPDATE tb_kon2_pen2 SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id_karyawan='$id2'";
        $qryup4p2 = mysqli_query($koneksi, $sqlup4p2);

        $sqlup4p3 = "UPDATE tb_kon2_pen3 SET mulai_penilaian='$mp3_2', selesai_penilaian='$sp3_2' WHERE id_karyawan='$id2'";
        $qryup4p3 = mysqli_query($koneksi, $sqlup4p3);

        $sqlup4p4 = "UPDATE tb_kon2_pen4 SET mulai_penilaian='$mp4_2', selesai_penilaian='$sp4_2' WHERE id_karyawan='$id2'";
        $qryup4p4 = mysqli_query($koneksi, $sqlup4p4);

        if ($row2['status_karyawan'] != "Kontrak 3") {
          $sqlkaryawan4 = "UPDATE tb_karyawan SET status_karyawan='Kontrak 2' WHERE id='$id2'";
          $qrykaryawan4 = mysqli_query($koneksi, $sqlkaryawan4);
        }

        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }
  }

  

  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin'];?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra2.png" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contract 2</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="viewass.php">View Assessment</a></li>
              <li class="breadcrumb-item"><a href="editass.php?nik<?php echo $row2['nik'];?>">Edit Assessment</a></li>
              <li class="breadcrumb-item active">Contract 2</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid col-8">
        <!-- /.row -->
        
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-olive">
          <div class="card-header">
            <h3 class="card-title">Change Contract 2</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form action="" method="post">
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label>NIK</label>
                    <input type="text" disabled class="form-control border-list-olive" value="<?php echo $row2['nik'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label>Assessment Status</label>
                    <select  name="status_karyawan" class="form-control select2" style="width: 100%;">
                      <?php if($row2['status_penilaian']){ ?>
                      <option selected="selected" value="<?php echo $row2['status_penilaian']; ?>">Assessment <?php echo $row2['status_penilaian'];?></option>
                      <?php }else{ ?><option selected="selected"></option><?php } ?>
                      <option value="1">Assessment 1</option>
                      <option value="2">Assessment 2</option>
                      <option value="3">Assessment 3</option>
                      <option value="4">Assessment 4</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-12">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Contract</label>
                      <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                        <input required name="mulai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate1" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['mulai_kontrak'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['mulai_kontrak']));}?>">
                        <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label>Finish Contract</label>
                      <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                        <input required name="selesai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate2" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['selesai_kontrak'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['selesai_kontrak']));}?>">
                        <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label>Sum Assessments</label>
                    <select name="banyak_penilaian" class="form-control select2" style="width: 100%;">
                      <?php if($row2['banyak_penilaian']){ ?>
                      <option selected="selected" value="<?php echo $row2['banyak_penilaian']; ?>"><?php echo $row2['banyak_penilaian'];?></option>
                      <?php }else{ ?><option selected="selected"></option><?php } ?>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 1</label>
                      <div class="input-group date" id="reservationdate3" data-target-input="nearest">
                        <input name="mp1" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate3" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['mulai_nilai1'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['mulai_nilai1']));}?>">
                        
                        <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 1</label>
                      <div class="input-group date" id="reservationdate4" data-target-input="nearest">
                        <input name="sp1" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate4" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['selesai_nilai1'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['selesai_nilai1']));}?>">
                        <div class="input-group-append" data-target="#reservationdate4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 2</label>
                      <div class="input-group date" id="reservationdate5" data-target-input="nearest">
                        <input name="mp2" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate5" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['mulai_nilai2'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['mulai_nilai2']));}?>">
                        <div class="input-group-append" data-target="#reservationdate5" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 2</label>
                      <div class="input-group date" id="reservationdate6" data-target-input="nearest">
                        <input name="sp2" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate6" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['selesai_nilai2'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['selesai_nilai2']));}?>">
                        <div class="input-group-append" data-target="#reservationdate6" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 3</label>
                      <div class="input-group date" id="reservationdate7" data-target-input="nearest">
                        <input name="mp3" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate7" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['mulai_nilai3'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['mulai_nilai3']));}?>">
                        <div class="input-group-append" data-target="#reservationdate7" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 3</label>
                      <div class="input-group date" id="reservationdate8" data-target-input="nearest">
                        <input name="sp3" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate8" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['selesai_nilai3'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['selesai_nilai3']));}?>">
                        <div class="input-group-append" data-target="#reservationdate8" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 4</label>
                      <div class="input-group date" id="reservationdate9" data-target-input="nearest">
                        <input name="mp4" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate9" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['mulai_nilai4'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['mulai_nilai4']));}?>">
                        <div class="input-group-append" data-target="#reservationdate9" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 4</label>
                      <div class="input-group date" id="reservationdate10" data-target-input="nearest">
                        <input name="sp4" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate10" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($row2['selesai_nilai4'] != "0000-00-00"){ echo date("d-m-Y", strtotime($row2['selesai_nilai4']));}?>">
                        <div class="input-group-append" data-target="#reservationdate10" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <input type="hidden" name="id" value="<?php echo $id;?>">
              <button onclick="return confirm('Apakah User Id yang anda buat sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
              <a href="editass.php?nik=<?php echo $row2['nik'];?>" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate1').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate2').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate3').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate4').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
     $('#reservationdate5').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate6').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate7').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate8').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate9').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    $('#reservationdate10').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent : false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>
